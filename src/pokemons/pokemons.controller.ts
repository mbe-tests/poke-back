import { Body, Controller, Delete, Get, HttpCode, Param, Patch, Post, Query } from '@nestjs/common';
import { Pokemon } from './pokemons.interface';
import { PokemonsService } from './pokemons.service';
import { CreateOrUpdatePokemonDto } from './pokemons-dto';

@Controller('pokemons')
export class PokemonsController {

    constructor(private readonly pokemonsService: PokemonsService) { }

    @Get()
    findAll(@Query('offset') offset: string, @Query('limit') limit: string): Pokemon[] {
        return this.pokemonsService.findAll(offset, limit);
    }

    @Get('/:id')
    find(@Param('id') id: string): Pokemon {
        return this.pokemonsService.find(id);
    }

    @Post()
    @HttpCode(201)
    create(@Body() createPokemonDto: CreateOrUpdatePokemonDto): Pokemon {
        return this.pokemonsService.create(createPokemonDto);
    }

    @Patch('/:id')
    update(@Body() UpdatePokemonDto: CreateOrUpdatePokemonDto, @Param('id') id): Pokemon  {
        return this.pokemonsService.update(UpdatePokemonDto, id);
    }

    @Delete('/:id')
    @HttpCode(204)
    delete(@Param('id') id) {
        return this.pokemonsService.delete(id);
    }
}
