export class CreateOrUpdatePokemonDto {
    'Name': string;
    'Type 1': string;
    'Type 2': string;
    'Total': number;
    'HP': number;
    'Attack': number;
    'Defense': number;
    'Sp. Atk': number;
    'Sp. Def': number;
    'Speed': number;
    'Generation': number;
    'Legendary': string;
}