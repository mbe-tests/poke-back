import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateOrUpdatePokemonDto } from './pokemons-dto';

import { Pokemon } from './pokemons.interface';
import * as POKEMON_BD from './pokemons.json';

@Injectable()
export class PokemonsService {

    pokemons: Array<Pokemon> = POKEMON_BD.map((pokemon, index) => ({ id: `${index}`, ...pokemon }));
    defaultLength = this.pokemons.length;


    findAll(offset: string = "0", limit: string = "10"): Pokemon[] {
        const start = parseInt(offset)
        const end = parseInt(limit) + start;
        return this.pokemons.slice(start, end);
    }
    find(id: string): Pokemon {
        const pokemon = this.pokemons.find(pokemon => pokemon.id === id);
        if (!pokemon) {
            throw new HttpException(`Error find pokemon ${id}`, HttpStatus.BAD_REQUEST);
        }
        return pokemon;
    }
    create(pokemonDto: CreateOrUpdatePokemonDto) {
        this.defaultLength = this.defaultLength + 1;
        const pokemon: Pokemon = {
            id: `${this.defaultLength}`,
            pokemonId: this.defaultLength,
            ...pokemonDto
        };
        this.pokemons.push(pokemon);
        return pokemon;
    }
    update(pokemonDto: CreateOrUpdatePokemonDto, id: string) {
        const index = this.pokemons.findIndex(pokemon => pokemon.id === id);
        if (index === -1) {
            throw new HttpException(`Error updating pokemon ${id}`, HttpStatus.BAD_REQUEST);
        }
        this.pokemons[index] = { ...this.pokemons[index], ... pokemonDto}
        return this.pokemons[index];
    }
    delete(id: any) {
        const index = this.pokemons.findIndex(pokemon => pokemon.id === id);
        if (index === -1) {
            throw new HttpException('Error deleting pokemon', HttpStatus.BAD_REQUEST);
        }
        this.pokemons.splice(index, 1);
        return;
    }
}
